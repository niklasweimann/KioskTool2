﻿namespace Updater
{
    partial class Updater
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CVLabelVersion = new System.Windows.Forms.Label();
            this.CVLabelSize = new System.Windows.Forms.Label();
            this.CVLabelBeschreibung = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.NVLabelVersion = new System.Windows.Forms.Label();
            this.NVLabelSize = new System.Windows.Forms.Label();
            this.NVLabelBeschreibung = new System.Windows.Forms.Label();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonUpdate = new System.Windows.Forms.Button();
            this.CVLableDate = new System.Windows.Forms.Label();
            this.NVLabelDate = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CVLableDate);
            this.groupBox1.Controls.Add(this.CVLabelVersion);
            this.groupBox1.Controls.Add(this.CVLabelSize);
            this.groupBox1.Controls.Add(this.CVLabelBeschreibung);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current Version:";
            // 
            // CVLabelVersion
            // 
            this.CVLabelVersion.AutoSize = true;
            this.CVLabelVersion.Location = new System.Drawing.Point(6, 29);
            this.CVLabelVersion.Name = "CVLabelVersion";
            this.CVLabelVersion.Size = new System.Drawing.Size(45, 13);
            this.CVLabelVersion.TabIndex = 5;
            this.CVLabelVersion.Text = "Version:";
            // 
            // CVLabelSize
            // 
            this.CVLabelSize.AutoSize = true;
            this.CVLabelSize.Location = new System.Drawing.Point(6, 16);
            this.CVLabelSize.Name = "CVLabelSize";
            this.CVLabelSize.Size = new System.Drawing.Size(30, 13);
            this.CVLabelSize.TabIndex = 4;
            this.CVLabelSize.Text = "Size:";
            // 
            // CVLabelBeschreibung
            // 
            this.CVLabelBeschreibung.AutoSize = true;
            this.CVLabelBeschreibung.Location = new System.Drawing.Point(6, 42);
            this.CVLabelBeschreibung.Name = "CVLabelBeschreibung";
            this.CVLabelBeschreibung.Size = new System.Drawing.Size(75, 13);
            this.CVLabelBeschreibung.TabIndex = 3;
            this.CVLabelBeschreibung.Text = "Beschreibung:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.NVLabelDate);
            this.groupBox2.Controls.Add(this.NVLabelVersion);
            this.groupBox2.Controls.Add(this.NVLabelSize);
            this.groupBox2.Controls.Add(this.NVLabelBeschreibung);
            this.groupBox2.Location = new System.Drawing.Point(259, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 180);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Newer Version:";
            // 
            // NVLabelVersion
            // 
            this.NVLabelVersion.AutoSize = true;
            this.NVLabelVersion.Location = new System.Drawing.Point(6, 29);
            this.NVLabelVersion.Name = "NVLabelVersion";
            this.NVLabelVersion.Size = new System.Drawing.Size(45, 13);
            this.NVLabelVersion.TabIndex = 8;
            this.NVLabelVersion.Text = "Version:";
            // 
            // NVLabelSize
            // 
            this.NVLabelSize.AutoSize = true;
            this.NVLabelSize.Location = new System.Drawing.Point(6, 16);
            this.NVLabelSize.Name = "NVLabelSize";
            this.NVLabelSize.Size = new System.Drawing.Size(30, 13);
            this.NVLabelSize.TabIndex = 7;
            this.NVLabelSize.Text = "Size:";
            // 
            // NVLabelBeschreibung
            // 
            this.NVLabelBeschreibung.AutoSize = true;
            this.NVLabelBeschreibung.Location = new System.Drawing.Point(6, 42);
            this.NVLabelBeschreibung.Name = "NVLabelBeschreibung";
            this.NVLabelBeschreibung.Size = new System.Drawing.Size(75, 13);
            this.NVLabelBeschreibung.TabIndex = 6;
            this.NVLabelBeschreibung.Text = "Beschreibung:";
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Location = new System.Drawing.Point(414, 198);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 1;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Location = new System.Drawing.Point(333, 198);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(75, 23);
            this.ButtonUpdate.TabIndex = 2;
            this.ButtonUpdate.Text = "Update";
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // CVLableDate
            // 
            this.CVLableDate.AutoSize = true;
            this.CVLableDate.Location = new System.Drawing.Point(6, 55);
            this.CVLableDate.Name = "CVLableDate";
            this.CVLableDate.Size = new System.Drawing.Size(33, 13);
            this.CVLableDate.TabIndex = 9;
            this.CVLableDate.Text = "Date:";
            // 
            // NVLabelDate
            // 
            this.NVLabelDate.AutoSize = true;
            this.NVLabelDate.Location = new System.Drawing.Point(6, 55);
            this.NVLabelDate.Name = "NVLabelDate";
            this.NVLabelDate.Size = new System.Drawing.Size(33, 13);
            this.NVLabelDate.TabIndex = 10;
            this.NVLabelDate.Text = "Date:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 233);
            this.Controls.Add(this.ButtonUpdate);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Updater";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label CVLabelVersion;
        private System.Windows.Forms.Label CVLabelSize;
        private System.Windows.Forms.Label CVLabelBeschreibung;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label NVLabelVersion;
        private System.Windows.Forms.Label NVLabelSize;
        private System.Windows.Forms.Label NVLabelBeschreibung;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Button ButtonUpdate;
        private System.Windows.Forms.Label CVLableDate;
        private System.Windows.Forms.Label NVLabelDate;
    }
}

