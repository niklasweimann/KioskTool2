﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace Updater
{
    public partial class Updater : Form
    {
        string path = @"C:\Daten\Login.exe";
        string server = "Http://127.0.0.1/";
        public Updater()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region Newer Version
            string xmlUrl = server + "update.xml";
            XmlTextReader reader = null;
            try
            {
                reader = new XmlTextReader(xmlUrl);
                reader.MoveToContent();
                string elementName = "";
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "appinfo"))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            elementName = reader.Name;
                        }
                        else
                        {
                            if ((reader.NodeType == XmlNodeType.Text) && (reader.HasValue))
                                switch (elementName)
                                {
                                    case "version":
                                        NVLabelVersion.Text = "Version: " + reader.Value;
                                        break;
                                    case "size":
                                        NVLabelSize.Text = "Size: " + reader.Value;
                                        break;
                                    case "about":
                                        NVLabelBeschreibung.Text = "Beschreibung: " + reader.Value;
                                        break;
                                    case "date":
                                        NVLabelDate.Text = "Date: " + reader.Value;
                                        break;

                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(1);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            #endregion
            #region Old Version
            CVLabelBeschreibung.Text = "Beschreibung: Nicht verfügbar.";
            //Size
            try
            {
                FileInfo fi = new FileInfo(path);
                CVLabelSize.Text = "Size: " + fi.Length + " Byte";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //Version
            try
            {
                FileVersionInfo myFI = FileVersionInfo.GetVersionInfo(path);
                Version applicationVersion = Version.Parse(myFI.FileVersion);
                CVLabelVersion.Text = "Version: " + Convert.ToString(applicationVersion);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //Date
            try
            {
                DateTime creation = File.GetCreationTime(path);
                CVLableDate.Text = "Date: " + Convert.ToString(creation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();

            Process P = new Process();
            P.StartInfo.FileName = path;
            P.Start();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            string downloadURL = "";
            Version newVersion = null;
            string aboutUpdate = "";
            string xmlUrl = server + "update.xml";
            XmlTextReader reader = null;
            try
            {
                reader = new XmlTextReader(xmlUrl);
                reader.MoveToContent();
                string elementName = "";
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "appinfo"))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            elementName = reader.Name;
                        }
                        else
                        {
                            if ((reader.NodeType == XmlNodeType.Text) && (reader.HasValue))
                                switch (elementName)
                                {
                                    case "version":
                                        newVersion = new Version(reader.Value);
                                        break;
                                    case "url":
                                        downloadURL = reader.Value;
                                        break;
                                    case "about":
                                        aboutUpdate = reader.Value;
                                        break;

                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(1);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            FileVersionInfo myFI = FileVersionInfo.GetVersionInfo(path);
            //Version applicationVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            Version applicationVersion = Version.Parse(myFI.FileVersion);
            if (applicationVersion.CompareTo(newVersion) < 0)
            {
                string str = String.Format("Neue Version gefunden!\nAktuelle Version: {0}.Neuste Version: {1}. \nChangelog: {2}. ", applicationVersion, newVersion, aboutUpdate);
                if (DialogResult.No != MessageBox.Show(str + "\nMöchtest du diese Update herunterladen?", "Updater - Update gefunden", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    try
                    {
                        Process.Start(downloadURL);
                    }
                    catch { }
                    return;
                }
                else
                {
                    ;
                }
            }
            else
            {
                MessageBox.Show("Aktuelle Version: " + applicationVersion + " Es gibt keine Updates.", "Updater - Kein Update erforderlich", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }
    }
}
