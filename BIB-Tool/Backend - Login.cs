﻿using System;
using System.Windows.Forms;

namespace BIB_Tool
{
    public partial class Backend : Form
    {
        public Backend()
        {
            InitializeComponent();
        }
        
        LogComponent mLogComponent = new LogComponent();
        XMLComponent mXMLComponent = new XMLComponent();
        CryptoComponent mCrypto = new CryptoComponent();

        // Schließt die Form beim klicken auf den cancelButton und erstellt einen Eintrag
        // in einer LOG-Datei.
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
            mLogComponent.Write("Backend - Login abgebrochen!");
        }

        // Überprüft, ob die eigegebenen Daten in den Textfeldern passwordTextBox und 
        // usernameTextBox in einer XML-Datei vorhanden sind und die Anmeldung somit 
        // erlaubt ist.
        private void loginButton_Click(object sender, EventArgs e)
        {
            //Verschlüsselt das eingegebene Passwort, nach den Methoden in der CryptoComponent-Klasse
            string cryptoPassword = mCrypto.Encode(passwordTextBox.Text);

            if (mXMLComponent.IsValidLogin(usernameTextBox.Text, passwordTextBox.Text) == true)
            {
                //Erstellt einen Eintrag in einer LOG-Datei
                mLogComponent.Write("Backend - Login erfolgreich!"); 

                //Zeigt Nach einer Erfolgreichen Anmeldung das Backend an und schließt diese Form
                Backend___Main backendMain = new Backend___Main();
                backendMain.Show();
                this.Close();
            }
            else
            {
                //Weißt den User durch eine Messagebox darauf hin, dass die Anmeldung fehlgeschlagen ist
                MessageBox.Show("User oder Passwort nicht gefunden.", "Falsches Passwort",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
