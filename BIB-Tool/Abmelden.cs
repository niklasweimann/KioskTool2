﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace BIB_Tool
{
    public partial class Abmelden : Form
    {
        XMLComponent mXMLComponent = new XMLComponent();
        LogComponent mLogComponent = new LogComponent();

        public Abmelden()
        {
            InitializeComponent();
        }

        private void Abmelden_Load(object sender, EventArgs e)
        {
            //Positioniert die Form in der rechten unteren Ecke des primären Bildschirms
            int Width = Screen.PrimaryScreen.Bounds.Width;
            int Height = Screen.PrimaryScreen.Bounds.Height;
            int tastbar = Screen.PrimaryScreen.Bounds.Height - Screen.PrimaryScreen.WorkingArea.Height;
            this.Location = new Point((Width - this.Width), (Height - (this.Height + tastbar)));

            //TODO: KeyLogger global
            timer1.Enabled = true;
            timer1.Start();

        }

        // Keylogger
        private void Abmelden_KeyUp(object sender, KeyEventArgs e)
        {
            if (mXMLComponent.ReadValueFromXML("KeyLoggerActive") == "True")
            {
                 if (mXMLComponent.ReadValueFromXML("KeyLoggerToLogFile") == "True")
                 {
                     mLogComponent.Write("Der User hat " + e.KeyCode + " gedrückt");
                 }
                 else
                 {
                     mLogComponent.WriteKeyLog(Convert.ToString(e.KeyCode));
                 }
             }
            

        }

        // Speichert das Ende der Nutzung in der LOG-Datei und wechselt zur Anmelden Form
        private void abmeldenButton_Click(object sender, EventArgs e)
        {
            mLogComponent.Write("User hat die Nutzung beendet.");

            Anmelden formAnmelden = new Anmelden();
            formAnmelden.Show();
            this.Hide();
        }

        // Verhindert, dass die Form verschoben werden kann
        private void Abmelden_LocationChanged(object sender, EventArgs e)
        {
            int Width = Screen.PrimaryScreen.Bounds.Width;
            int Height = Screen.PrimaryScreen.Bounds.Height;
            int tastbar = Screen.PrimaryScreen.Bounds.Height - Screen.PrimaryScreen.WorkingArea.Height;
            this.Location = new Point((Width - this.Width), (Height - (this.Height + tastbar)));
        }

        // Verhindert, dass die Form geschlossen werden kann
        private void Abmelden_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }
    }
}
