﻿using System;
using System.IO;
using System.Windows.Forms;

namespace BIB_Tool
{
    public partial class Anmelden : Form
    {
        XMLComponent mXMLComponent = new XMLComponent();
        LogComponent mLogComponent = new LogComponent();

        private string pathLogPath = "C:\\Daten\\Logfiles\\";

        public bool canBeCloesed { get; set; }


        public Anmelden()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Entfernt den Rahmen der Form
            this.FormBorderStyle = FormBorderStyle.None;
            // Setzt die Form immer in den Vordergrund
            this.TopMost = true;
            this.Bounds = Screen.PrimaryScreen.Bounds;

            // Erstellt das Verzeichnis für die LOG-Date
            if (!Directory.Exists(pathLogPath))
            {
                Directory.CreateDirectory(pathLogPath);
            }
            this.CreateFileWatcher("C:\\Daten\\Logfiles\\AGBlogfile.log");
            timer1.Enabled = true;
            timer1.Start();
        }

        // Verhindert, dass die Form geschlossen werden kann
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown || canBeCloesed == true)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        // Dokumentiert, wenn der User auf acceptButton klickt und zeigt die Form Abmelden
        private void acceptButton_Click(object sender, EventArgs e)
        {
            mLogComponent.Write("User hat die Nutzung gestartet.");
            this.Hide();
            Abmelden formAbmelden = new Abmelden();
            formAbmelden.Show();
        }

        #region FileWatcher
        public void CreateFileWatcher(string path)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            //Legt den zu überwachenden Pfad fest
            watcher.Path = @"C:\";

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            watcher.Filter = "*.*";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnRenamed);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            switch (e.FullPath)
            {
                case @"C:\Config.MSI": break;
                case @"C:\Config.msi": break;
                default: mLogComponent.Write("Datei: " + e.FullPath + " " + e.ChangeType); break;
            }
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            string toLog = (string)("Datei: "+ e.OldFullPath + " wurde in " + e.FullPath  + " umbennant");
            mLogComponent.Write(toLog);
        }
        #endregion

        // Fährt das System herunter
        private void shortShutdownButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(System.Environment.SystemDirectory + "\\shutdown.exe", "-s -t 0");
        }


        // Öffnet die Backend-Login Form, nachdem "ALT + Y" gedrückt wurde
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (Control.ModifierKeys == Keys.Alt &&  e.KeyCode == Keys.Y)
            {
                this.TopMost = false;
                Backend backendForm = new Backend();
                backendForm.Show();
                mLogComponent.Write("Backend - Login geöffnet!");
            }
        }

        // Keylogger, der alle Eingaben in eine Datei schreibt
        private void Anmelden_KeyUp(object sender, KeyEventArgs e)
        {
            if (mXMLComponent.ReadValueFromXML("KeyLoggerActive") == "True")
            {
                if (mXMLComponent.ReadValueFromXML("KeyLoggerToLogFile") == "True")
                {
                    mLogComponent.Write("Der User hat " + e.KeyCode + " gedrückt");
                }
                else
                {
                    mLogComponent.WriteKeyLog(Convert.ToString(e.KeyCode));
                }
            }
        }
    }
}
