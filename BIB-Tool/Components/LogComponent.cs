﻿using System;
using System.IO;

namespace BIB_Tool
{
    class  LogComponent
    {
        /// <summary>
        /// Schreibt ein Ereignis in die LogFile.log Datei
        /// </summary>
        /// <param name="Ereignis">Ereignis, dass in die Logdatei geschrieben werden soll</param>
        public void Write(string Ereignis)
        {
            StreamWriter file = new StreamWriter("C:\\Daten\\Logfiles\\LogFile.log", true);
            file.WriteLine(this.getDateAndTime()+" " + Ereignis);
            file.Close();
        }

        /// <summary>
        /// Schreibt einen Char in die KeyLogger.log Datei
        /// </summary>
        /// <param name="pKey">Zeichen, dass in die KeyLogger.log geschrieben werden soll</param>
        public void WriteKeyLog(string pKey)
        {
            StreamWriter file = new StreamWriter("C:\\Daten\\Logfiles\\KeyLogger.log", true);
            file.WriteLine(this.getDateAndTime() + " " + pKey);
            file.Close();
        }

        // Erstellt einen String nach dem Schema: [dd.MM.yyy hh.mm.ss]
        #region Zeit und Datum erstellen
        public string getTime()
        {
            DateTime saveNow = DateTime.Now;
            string stunde = Convert.ToString(DateTime.Now.ToString("HH"));
            string minute = Convert.ToString(DateTime.Now.ToString("mm"));
            string sekunde = Convert.ToString(DateTime.Now.ToString("ss"));

            string Ausgabe = stunde + ":" + minute + ":" + sekunde;
            return Ausgabe;
        }

        public string getDate()
        {
            return DateTime.Now.ToString("dd.MM.yyyy");
        }

        public string getDateAndTime()
        {
            string Time = this.getTime();
            string Date = this.getDate();

            return "[" + Date + " " + Time + "]";
        }
        #endregion
    }
}
