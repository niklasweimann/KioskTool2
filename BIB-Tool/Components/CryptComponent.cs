﻿using System;
using System.Text;

namespace BIB_Tool
{
    internal sealed class CryptoComponent
    {
        /// <summary>
        /// Verschlüsselt einen String
        /// </summary>
        /// <param name="str">String, der verschlüsselt werden soll</param>
        /// <returns>Verschlüsselter String</returns>
        public string Encode(string str)
        {
            byte[] encbuff = Encoding.UTF8.GetBytes(str);
            return Convert.ToBase64String(encbuff);
        }

        /// <summary>
        /// Entschlüsselt einen String
        /// </summary>
        /// <param name="str">String, der entschlüsselt werden soll</param>
        /// <returns>Entschlüsselter String</returns>
        public string Decode(string str)
        {
            byte[] decbuff = Convert.FromBase64String(str);
            return Encoding.UTF8.GetString(decbuff);
        }
    }
}
