﻿using System.Xml;
using System.Xml.XPath;

namespace BIB_Tool
{
    class XMLComponent
    {
        string connectionString;

        //Eigenschaften
        public string benuter { get; private set; }
        public string password { get; private set; }
        public int  rights { get; private set; }
        public string loginFilePath { get { return loginPath; } set { loginPath = "C:\\Daten\\Login.xml"; } }

        // Pfade
        static string settingsFilePath = "C:\\Daten\\settings.xml";
        private string loginPath = "C:\\Daten\\Login.xml";

        CryptoComponent mCrypto = new CryptoComponent();
       
        public void nodeName(string name)
        {
              connectionString = ReadValueFromXML(name);
        }
  
        /// <summary>
        /// Reads the data of specified node provided in the parameter
        /// </summary>
        /// <param name="pstrValueToRead">Node to be read</param>
        /// <returns>string containing the value</returns>
        public string ReadValueFromXML(string pstrValueToRead)
        {
            try
            {
                //settingsFilePath is a string variable storing the path of the settings file 
                XPathDocument doc = new XPathDocument(settingsFilePath);
                XPathNavigator nav = doc.CreateNavigator();
                // Compile a standard XPath expression
                XPathExpression expr;
                expr = nav.Compile(@"/settings/" + pstrValueToRead);
                XPathNodeIterator iterator = nav.Select(expr);
                // Iterate on the node set
                while (iterator.MoveNext())
                {
                    return iterator.Current.Value;
                }
                return string.Empty;
            }
            catch
            {
                //do some error logging here. Leaving for you to do 
                return string.Empty;
            }
        }
  
        /// <summary>
        /// Writes the updated value to XML
        /// </summary>
        /// <param name="pstrValueToRead">Node of XML to read</param>
        /// <param name="pstrValueToWrite">Value to write to that node</param>
        /// <returns></returns>
        public bool WriteValueTOXML(string pstrValueToRead, string pstrValueToWrite)
        {
            try
            {
                //settingsFilePath is a string variable storing the path of the settings file 
                XmlTextReader reader = new XmlTextReader(settingsFilePath);
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);
                //we have loaded the XML, so it's time to close the reader.
                reader.Close();
                XmlNode oldNode;
                XmlElement root = doc.DocumentElement;
                oldNode = root.SelectSingleNode("/settings/" + pstrValueToRead);
                oldNode.InnerText = pstrValueToWrite;
                doc.Save(settingsFilePath);
                return true;
            }
            catch
            {
                //properly you need to log the exception here. But as this is just an
                //example, I am not doing that. 
                return false;
            }
        }
  
        /// <summary>
        /// Testet, ob die übergebenen Daten mit denen in der Login.xml übereinstimmen
        /// </summary>
        /// <param name="pUser">Username, der geprüft werden soll</param>
        /// <param name="pPassword">Passwort, das geprüft werden soll</param>
        /// <returns></returns>
        public bool IsValidLogin(string pUser, string pPassword)
        {
            XmlDocument myXml = new XmlDocument();
            myXml.Load(loginPath);
            XmlNodeList userList = myXml.SelectNodes("root / user");
  
            foreach (XmlNode user in userList)
            {
                benuter = user["username"].InnerText;
                password = user["password"].InnerText;
  
                if (benuter == pUser && password == mCrypto.Encode(pPassword))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
