﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Net.NetworkInformation;

namespace BIB_Tool.Components
{
    internal sealed class FTPComponent
    {
        public string lFilePath { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }


        WebClient client = new WebClient();

        public void uploadFile(string plFilePath, string pUserName, string pPassword, string pServer)
        {
              lFilePath = plFilePath;
              UserName = pUserName;
              Password = pPassword;
              Server = pServer;

              client.Credentials = new NetworkCredential(UserName, Password);
              client.UploadFile(pServer + "/" + new FileInfo(lFilePath).Name, "STOR", lFilePath);
        }

        public bool CheckInternetConnection(string pURL)
        {
            Ping ping = new Ping();

            try
            {
                PingReply reply = ping.Send(pURL, 500);
                return reply.Status == IPStatus.Success;
            }
            catch
            {
                return false;
            }
        }
    }
}
