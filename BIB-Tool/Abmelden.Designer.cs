﻿namespace BIB_Tool
{
    partial class Abmelden
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.abmeldenButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // abmeldenButton
            // 
            this.abmeldenButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abmeldenButton.Location = new System.Drawing.Point(0, 0);
            this.abmeldenButton.Name = "abmeldenButton";
            this.abmeldenButton.Size = new System.Drawing.Size(262, 56);
            this.abmeldenButton.TabIndex = 0;
            this.abmeldenButton.Text = "Abmelden";
            this.abmeldenButton.UseVisualStyleBackColor = true;
            this.abmeldenButton.Click += new System.EventHandler(this.abmeldenButton_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Abmelden
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 56);
            this.Controls.Add(this.abmeldenButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Abmelden";
            this.Text = "Abmelden";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Abmelden_FormClosing);
            this.Load += new System.EventHandler(this.Abmelden_Load);
            this.LocationChanged += new System.EventHandler(this.Abmelden_LocationChanged);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Abmelden_KeyUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button abmeldenButton;
        private System.Windows.Forms.Timer timer1;
    }
}